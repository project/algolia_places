## Algolia Places

This module integrates the Algolia Places JavaScript library
into Drupal. It provides a widget for text inputs on entity
forms to leverage the Algolia Places JS library.

To learn more about Algolia Places, read this:
https://community.algolia.com/places/

### Algolia Updates

https://www.algolia.com/blog/product/sunsetting-our-places-feature/

### Algolia Documentation

https://community.algolia.com/places/documentation.html

### Requirements

No requirements at this time.

### Install

* Install the module.
* Clear cache.
* Go to Manage > Form Display.
* Change a textfield field and select the Algolia Places widget.

### Roadmap

* Add in configuration fields for APP_ID & API_KEY.

### Maintainers

* George Anderson (geoanders)
https://www.drupal.org/u/geoanders
